-- Requêtes d’interrogation de la base de données : 
USE asterix;
-- 1. Liste des potions : Numéro, libellé, formule et constituant principal. (5 lignes) 
SELECT "Liste des potions" AS "Question 1:";
SELECT * FROM potion;

-- 2. Liste des noms des trophées rapportant 3 points. (2 lignes) 
SELECT "Liste des noms des trophées rapportant 3 points" AS "Question 2:";
SELECT `CodeCat`  FROM categorie WHERE `NbPoints` = "3";

-- 3. Liste des villages (noms) contenant plus de 35 huttes.  (4 lignes) 
SELECT "Liste des villages (noms) contenant plus de 35 huttes" AS "Question 3:";
SELECT `NomVillage` FROM village WHERE `NbHuttes` > "35";

-- 4. Liste des trophées (numéros) pris en mai / juin 52.  (4 lignes) 
SELECT "Liste des trophées (numéros) pris en mai / juin 52" AS "Question 4:";
SELECT `NumTrophee`FROM trophee WHERE `DatePrise` >= "2052-05-01" and `DatePrise` <= "2052-06-30" ;

-- 5. Noms des habitants commençant par 'a' et contenant la lettre 'r'. (3 lignes)
SELECT "Noms des habitants commençant par 'a' et contenant la lettre 'r'" AS "Question 5:";
SELECT `Nom` FROM habitant WHERE `Nom` LIKE 'a%r%';

-- 6. Numéros des habitants ayant bu les potions numéros 1, 3 ou 4. (8 lignes)
SELECT "Numéros des habitants ayant bu les potions numéros 1, 3 ou 4" AS "Question 6:";
SELECT DISTINCT `NumHab` FROM absorber 
WHERE (`NumPotion` IN ("1", "3", "4"));

-- 7. Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur. (10 lignes)
SELECT "Liste des trophées" AS "Question 7:";
SELECT `NumTrophee`, `DatePrise`, `NomCateg`, `Nom` as NomPreneur 
FROM trophee 
INNER JOIN categorie ON trophee.`CodeCat` = categorie.`CodeCat`
INNER JOIN habitant ON trophee.`NumPreneur` = habitant.`NumHab`;

-- 8. Nom des habitants qui habitent à Aquilona. (7 lignes)
SELECT "Nom des habitants qui habitent à Aquilona" AS "Question 8:";
SELECT Nom FROM habitant
JOIN village ON habitant.`NumVillage` = village.`NumVillage`
WHERE village.`NomVillage` = "Aquilona";

-- 9. Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat. (2 lignes) 
SELECT "Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat" AS "Question 9:";
SELECT habitant.`Nom`FROM habitant
JOIN trophee ON habitant.`NumHab` = trophee.`NumPreneur`
JOIN categorie ON trophee.`CodeCat` = categorie.`CodeCat`
WHERE categorie.`NomCateg` = "Bouclier de Légat";

-- 10. Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant
-- principal. (3 lignes) 
SELECT "Liste des potions fabriquées par Panoramix " AS "Question 10:";
SELECT LibPotion, Formule, ConstituantPrincipal FROM potion
JOIN fabriquer ON potion.`NumPotion` = fabriquer.`NumPotion`
JOIN habitant ON habitant.`NumHab` = fabriquer.`NumHab`
WHERE habitant.`Nom` = "Panoramix";

-- 11. Liste des potions (libellés)  absorbées par Homéopatix.  (2 lignes)
SELECT "iste des potions absorbées par Homéopatix" AS "Question 11:";
SELECT LibPotion FROM potion
JOIN absorber ON potion.`NumPotion` = absorber.`NumPotion`
JOIN habitant ON habitant.`NumHab` = absorber.`NumHab`
WHERE habitant.`Nom` = "Homéopatix";

-- 12. Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro 3. (4 lignes)
SELECT "Liste des habitants ayant absorbé une potion fabriquée par l'habitant numéro 3" AS "Question 12:";
SELECT DISTINCT habitant.`Nom` FROM absorber  -- correction DISTINCT manquant
JOIN habitant ON absorber.`NumHab` = habitant.`NumHab`
JOIN fabriquer ON absorber.`NumPotion` = fabriquer.`NumPotion`  -- correction numpotion non testé
WHERE fabriquer.`NumHab` = 3; -- correction ""

-- 13. Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix. (7 lignes)
SELECT "Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix" AS "Question 13:";
SELECT DISTINCT habitant.`Nom` FROM absorber
JOIN habitant ON absorber.`Numhab` = habitant.`NumHab`
JOIN fabriquer ON absorber.`NumPotion` = fabriquer.`NumPotion`
WHERE fabriquer.`NumHab` IN (
    SELECT fabriquer.`NumHab` FROM fabriquer -- `Age`
    JOIN habitant ON habitant.`NumHab` = fabriquer.`NumHab` -- Join inutile car numhab est identique
    WHERE habitant.`Nom` = "Amnésix");

-- 14. Nom des habitants dont la qualité n'est pas renseignée. (3 lignes) 
SELECT "Nom des habitants dont la qualité n'est pas renseignée." AS "Question 14:";
SELECT `Nom` FROM habitant
WHERE `NumQualite` is NULL;

-- 15. Nom des habitants ayant consommé la potion magique n°1 (c'est le libellé de la 
-- potion) en février 52.  (3 lignes)
SELECT "Nom des habitants ayant consommé la potion magique n°1 en février 52" AS "Question 15:";
SELECT habitant.`Nom` FROM absorber
JOIN habitant ON absorber.`NumHab` = habitant.`NumHab`
WHERE (absorber.`DateA` BETWEEN "2052-02-01" AND "2052-02-29") AND absorber.`NumPotion` IN (
    SELECT DISTINCT absorber.`NumPotion` FROM absorber 
    JOIN potion ON potion.`NumPotion` = absorber.`NumPotion`
    WHERE potion.`LibPotion` = "Potion magique n°1");

-- 16. Nom et âge des habitants par ordre alphabétique. (22 lignes)
SELECT "Nom et âge des habitants par ordre alphabétique" AS "Question 16:";
SELECT `Nom`, `Age` FROM habitant
ORDER BY `Nom` ASC;

-- 17. Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom du village. (3 lignes) 
SELECT "Liste des resserres classées de la plus grande à la plus petite" AS "Question 17:";
SELECT resserre.`NomResserre`, village.`NomVillage` FROM resserre
JOIN village ON village.`NumVillage` = resserre.`NumVillage`
ORDER BY `NomResserre` ASC;

-- *** 
-- 18. Nombre d'habitants du village numéro 5.  (4)
SELECT "Nombre d'habitants du village numéro 5" AS "Question 18:";
SELECT COUNT(habitant.`Nom`) AS NbHabitant FROM habitant
WHERE `NumVillage` = "5";

-- 19. Nombre de points gagnés par Goudurix. (5)
SELECT "Nombre de points gagnés par Goudurix" AS "Question 19:";
SELECT SUM(categorie.`NbPoints`) FROM trophee
JOIN categorie ON trophee.`CodeCat` = categorie.`CodeCat`
WHERE trophee.`NumPreneur` IN (
    SELECT habitant.`NumHab` FROM habitant
    WHERE habitant.`Nom` = "Goudurix");

-- 20. Date de première prise de trophée. (03/04/52)
SELECT "Date de première prise de trophée" AS "Question 20:";
SELECT MIN(trophee.`DatePrise`) FROM trophee;

-- 21. Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées. (19)
SELECT "Nombre de louches de potion magique n°2 absorbées" AS "Question 21:";
SELECT SUM(absorber.`Quantite`) FROM absorber
WHERE absorber.`NumPotion` IN (
    SELECT potion.`NumPotion` FROM potion
    WHERE potion.`LibPotion` = "Potion magique n°2");

-- 22. Superficie la plus grande. (895) 
SELECT "Superficie la plus grande" AS "Question 22:";
SELECT MAX(resserre.`Superficie`) FROM resserre;

-- *** 
-- 23. Nombre d'habitants par village (nom du village, nombre). (7 lignes)
SELECT "Nombre d'habitants par village" AS "Question 23:";
SELECT village.`NomVillage`, COUNT(village.`NomVillage`) AS NbHabitant FROM habitant
JOIN village ON habitant.`NumVillage` = village.`NumVillage`
GROUP BY village.`NomVillage`;

-- 24. Nombre de trophées par habitant (6 lignes)
SELECT "Nombre de trophées par habitant" AS "Question 24:";
SELECT habitant.`Nom`, COUNT(trophee.`NumTrophee`) AS NbTrophee FROM habitant
JOIN trophee ON habitant.`NumHab` = trophee.`NumPreneur`
GROUP BY habitant.`Nom`;

-- 25. Moyenne d'âge des habitants par province (nom de province, calcul). (3 lignes)
SELECT "Moyenne d'âge des habitants par province" AS "Question 25:";
-- Fonctionne pas
-- SELECT province.`NomProvince`, village.`NomVillage`, habitant.`Age` FROM province
-- JOIN village ON province.`NumProvince` = village.`NumProvince`
-- JOIN habitant ON village.`NumVillage` = habitant.`NumVillage`
-- ORDER BY province.`NomProvince`;

-- Faire plusieurs requètes
-- 1 Liste des provinces
-- SELECT province.`NomProvince` FROM province;
-- 2 liste villages par province
-- SELECT province.`NomProvince`, GROUP_CONCAT(village.`NomVillage`) FROM village
-- JOIN province ON village.`NumProvince` = province.`NumProvince`
-- GROUP BY province.`NomProvince`;

-- 3 moyenne age par village
SELECT province.`NomProvince`, AVG(habitant.`Age`) FROM village
JOIN province ON village.`NumProvince` = province.`NumProvince`
JOIN habitant ON village.`NumVillage` = habitant.`NumVillage`
GROUP BY province.`NomProvince`;


-- 26. Nombre de potions différentes absorbées par chaque habitant (nom et nombre). (9 lignes)
SELECT "Nombre de potions différentes absorbées par chaque habitant" AS "Question 26:";
SELECT habitant.`Nom`, SUM(absorber.`Quantite`) AS NbPotionAbsorber FROM absorber
JOIN habitant ON habitant.`NumHab` = absorber.`NumHab`
GROUP BY habitant.`Nom`;


-- 27. Nom des habitants ayant bu plus de 2 louches de potion zen. (1 ligne) 
SELECT "Nombre de potions différentes absorbées par chaque habitant" AS "Question 27:";
SELECT habitant.`Nom`, absorber.`NumPotion`, absorber.`Quantite` FROM absorber
JOIN habitant ON habitant.`NumHab` = absorber.`NumHab`
WHERE absorber.`Quantite` > "2" AND absorber.`NumPotion` IN (
    SELECT absorber.`NumPotion` FROM absorber
    JOIN potion ON potion.`NumPotion` = absorber.`NumPotion`
    WHERE potion.`LibPotion` = "Potion Zen");

-- *** 
-- 28. Noms des villages dans lesquels on trouve une resserre (3 lignes) 
SELECT "Noms des villages dans lesquels on trouve une resserre" AS "Question 28:";
SELECT village.`NomVillage`, resserre.`NomResserre` FROM village
JOIN resserre ON resserre.`NumVillage` = village.`NumVillage`;

-- 29. Nom du village contenant le plus grand nombre de huttes. (Gergovie)
SELECT "Nom du village contenant le plus grand nombre de huttes" AS "Question 29:";
-- Requète par sous-requète MAX:
SELECT village.`NomVillage`, village.`NbHuttes` FROM village
WHERE village.`NbHuttes` IN (SELECT MAX(village.`NbHuttes`) FROM village);

-- Requète par fonction LIMIT et ORDER BY
SELECT village.`NomVillage`, village.`NbHuttes` FROM village
ORDER BY village.`NbHuttes` DESC
LIMIT 1;

-- 30. Noms des habitants ayant pris plus de trophées qu'Obélix. (3 lignes)
SELECT "Noms des habitants ayant pris plus de trophées qu'Obélix" AS "Question 30:";
SELECT habitant.`Nom`, COUNT(categorie.`NbPoints`) AS NbPointsTrophee FROM habitant
JOIN trophee ON habitant.`NumHab` = trophee.`NumPreneur`
JOIN categorie ON trophee.`CodeCat` = categorie.`CodeCat`
GROUP BY habitant.`Nom`
HAVING NbPointsTrophee > (
    SELECT COUNT(categorie.`NbPoints`) FROM habitant
    JOIN trophee ON habitant.`NumHab` = trophee.`NumPreneur`
    JOIN categorie ON trophee.`CodeCat` = categorie.`CodeCat`
    WHERE habitant.`Nom` = "Obélix");